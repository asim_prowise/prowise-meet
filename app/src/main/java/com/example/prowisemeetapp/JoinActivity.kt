package com.example.prowisemeetapp

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_join.*
import org.jitsi.meet.sdk.JitsiMeetActivity
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions
import java.net.URL

class JoinActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join)

        joinBtn.setOnClickListener {
            var name = roomName.text.toString()
            if (name.isNotEmpty()) {
                try {
                    val serverURL = URL("https://meet.jit.si")
                    val builder = JitsiMeetConferenceOptions.Builder()
                    builder.setServerURL(serverURL)
                    builder.setWelcomePageEnabled(false)
                    builder.setVideoMuted(true)
                    builder.setAudioMuted(true)
                    builder.setRoom(name)

                    JitsiMeetActivity.launch(this@JoinActivity, builder.build())
                    finish()
                } catch (e: Exception) {
                    Toast.makeText(this@JoinActivity, e.message, Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        }

    }
}