package com.example.prowisemeetapp.models

import java.io.Serializable

class User : Serializable {
    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
    var token: String? = null
}