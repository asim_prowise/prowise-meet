package com.example.prowisemeetapp.listeners

import com.example.prowisemeetapp.models.User

interface UsersListener {

    fun initiateAudioMeeting(user: User)

    fun initiateVideoMeeting(user: User)

    fun onMultipleUsersAction(isMultipleUsersSelected: Boolean)

}